# Правила по умолчанию
iptables -P INPUT DROP
iptables -P FORWARD DROP
iptables -P OUTPUT ACCEPT
# Разрешить входящие соединения с локалхоста
iptables -A INPUT -i lo -j ACCEPT
# Разрешить уже установленные входящие соединения
iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
# Разрешить SSH
iptables -A INPUT -p TCP --dport 202 -j ACCEPT
# Разрешить  порт
iptables -A INPUT -p TCP --dport 80 -j ACCEPT
iptables -A INPUT -p TCP --dport 443 -j ACCEPT
iptables -A INPUT -p TCP --dport 4043 -j ACCEPT
#
#
#iptables -A INPUT -p TCP --dport 0 -j ACCEPT
